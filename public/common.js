async function loadRIJZ() {
    const ds = new DecompressionStream('gzip');
    let url = (location.href.includes('/pritozba') ? '../' : '') + '../data/rzijz.xml.gz'
    const resp = await fetch(url);
    const blob_in = await resp.blob();
    const stream_in = blob_in.stream().pipeThrough(ds);
    const blob_out = await new Response(stream_in).blob();
    const xml = await blob_out.text();
    
    const parser = new DOMParser();
    const doc = parser.parseFromString(xml, "application/xml");
    window.RIJZ = doc;

    let $search = document.getElementById('autofill-search');
    $search.disabled = false;
    $search.focus();

    $search.dispatchEvent(new Event('input'));
}

function txt($el) {
    return $el ? $el.textContent : '';
}

function searchRIJZ(search, limit=undefined) {
    let $nazivi = window.RIJZ.children[0].querySelectorAll("Zavezanec > Naziv");
    search = search.toLowerCase();
    
    let results = [];
    for (let i = 0; i < $nazivi.length; i++) {
        let $naziv = $nazivi[i];
        let naziv = $naziv.textContent.toLowerCase();
        if (naziv.includes(search)) {
            let $ulica = $naziv.parentElement.querySelector("Naslov > Ulica");
            let $hisnaStevilka = $naziv.parentElement.querySelector("Naslov > HisnaStevilka");
            let $hisnaStevilkaDodatek = $naziv.parentElement.querySelector("Naslov > HisnaStevilkaDodatek");
            let $posta = $naziv.parentElement.querySelector("Naslov > Posta");
            let $postnaSt = $naziv.parentElement.querySelector("Naslov > PostnaSt");
            let $eNaslov = $naziv.parentElement.querySelector("ENaslov");

            results.push({
                naziv: txt($naziv),
                ulica: `${txt($ulica)} ${txt($hisnaStevilka)}${txt($hisnaStevilkaDodatek)}`,
                posta: `${txt($postnaSt)} ${txt($posta)}`,
                eposta: txt($eNaslov),
            })

            if (limit !== undefined && results.length >= limit) {
                break;
            }
        }
    }

    return results;
}


function init_autofill() {
    document.getElementById('autofill-btn').addEventListener('click', async () => {
        if (window.RIJZ === undefined) {
            loadRIJZ();
        }
        document.getElementById('autofill-dialog').showModal()
    })

    document.getElementById('autofill-search').addEventListener('input', () => {
        let search = document.getElementById('autofill-search').value
        let $results = document.getElementById('autofill-results');
        $results.innerHTML = ''
        
        let results = searchRIJZ(search, limit=1000);

        let $_results = "";
        results.forEach((result) => {
            $_results += `<div class="result" data-naziv="${result.naziv}" data-ulica="${result.ulica}"" data-posta="${result.posta}" data-eposta="${result.eposta}">${result.naziv}</div>`
        })
        $results.innerHTML = $_results
    })

    document.getElementById('autofill-results').addEventListener('click', (event) => {
        let $result = event.target;
        let naziv = $result.dataset.naziv;
        let ulica = $result.dataset.ulica;
        let posta = $result.dataset.posta;
        let eposta = $result.dataset.eposta;

        document.getElementById('zavezanec').value = `${naziv}\n${ulica}\n${posta}\n${eposta}`;
        document.getElementById('zavezanec').dispatchEvent(new Event('input'));
        document.getElementById('autofill-dialog').close();
    })
}

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('localStorageProsilecInfoDelete-btn').addEventListener('click', () => {
        try {
            let prosilec = JSON.parse(localStorage.getItem('zdijz-prosilec'))
            // check what is selected
            let selectedIndex = document.getElementById('localStorageProsilec-select').value
            if (selectedIndex === '') {
                alert('Prosilec ni izbran, prosim izberite prosilca za brisanje.')
                return
            } 
            // if it's in the text area, remove it
            let selectedData = prosilec[selectedIndex]
            if (selectedData) {
                let prosilecText = document.getElementById('prosilec').value
                if (prosilecText === selectedData.data) {
                    document.getElementById('prosilec').value = ''
                }
            }
            // remove selected item
            prosilec.splice(selectedIndex, 1)
            localStorage.setItem('zdijz-prosilec', JSON.stringify(prosilec))
            loadData();
        } catch (e) {
            console.error(e)
        }
    })
    
    document.getElementById('localStorageProsilecInfoSave-btn').addEventListener('click', () => {
        let prosilec = document.getElementById('prosilec').value
        let name = prompt('Vnesite ime pod katerim želite shraniti podatke')
        if (!name) {
            return
        }
        let existing_data = JSON.parse(localStorage.getItem('zdijz-prosilec')) || []
        existing_data.push({name, data: prosilec})
        localStorage.setItem(`zdijz-prosilec`, JSON.stringify(existing_data))
        loadData();
        // set as selected
        let $select = document.getElementById('localStorageProsilec-select')
        $select.value = existing_data.length - 1
    })

    // load data into localStorageProsilec-select
    loadData();

    function loadData() {
        let $select = document.getElementById('localStorageProsilec-select')
        let data = JSON.parse(localStorage.getItem('zdijz-prosilec')) || []
        // clear out existing options
        $select.innerHTML = ''
        let options = ''
        // add empty option as default
        options += `<option value="">Izberi shranjenega prosilca</option>`
        data.forEach((item, i) => {
            options += `<option value="${i}">${item.name}</option>`
        })
        $select.innerHTML = options
    }

    document.getElementById('localStorageProsilec-select').addEventListener('change', (event) => {
        let selectedIndex = event.target.value
        if (selectedIndex === '') {
            return
        }
        let data = JSON.parse(localStorage.getItem('zdijz-prosilec')) || []
        let selectedData = data[selectedIndex]
        if (selectedData) {
            document.getElementById('prosilec').value = selectedData.data
        }
        // trigger input event
        document.getElementById('prosilec').dispatchEvent(new Event('input'))
    })
});