#!/bin/sh

mkdir -p public/data

# Download RIJZ
wget 'https://www.ajpes.si/RZIJZ/Vpogled/XML' -O public/data/rzijz.xml

# Compress it for faster loading
gzip -f public/data/rzijz.xml
